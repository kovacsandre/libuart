#include <avr/io.h>
#include <util/setbaud.h>
#include "uart.h"

void
USART0_Init(void)
{
    /* Set baud rate Asynchronous Normal mode
     * defined in setbaud.h you can change baudrate in Makefile
     */
    UBRR0H = UBRRH_VALUE;
    UBRR0L = UBRRL_VALUE;
    /* Set frame format: 8data, no parity & 1 stop bits */
    UCSR0C = (0<<UMSEL0) | (0<<UPM0) | (0<<USBS0) | (3<<UCSZ0);
    /* Enable receiver and transmitter */
    UCSR0B = (1<<RXEN0) | (1<<TXEN0);
}

void
USART0_Transmit(uint8_t data)
{
    /* Wait for empty transmit buffer */
    while (!(UCSR0A & (1<<UDRE0)));
    /* Put data into buffer, sends the data */
    UDR0 = data;
}


void
USART0_send_msg(uint8_t *msg)
{
    while (*msg) USART0_Transmit(*msg++);
}
