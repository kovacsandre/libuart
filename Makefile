TARGET = uart
MCU = at90can32
F_CPU = 16000000UL
UART_BAUD = 9600UL

CFLAGS = -D F_CPU=$(F_CPU) -DBAUD=$(UART_BAUD) -mmcu=$(MCU) -Os -funsigned-char -Wall

%.a: %.o
	avr-ar ru $(TARGET).a $< $@

%.o: %.c
	avr-gcc -c $(CFLAGS) $< -o $@

all: $(TARGET).a

clean:
	rm -f *.o
	rm -f *.a

.PHONY: all clean
